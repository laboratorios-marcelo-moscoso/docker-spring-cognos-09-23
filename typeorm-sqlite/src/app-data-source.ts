import { DataSource } from "typeorm"

export const AppDataSource = new DataSource({
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: "postgres",
    password: "mypassword",
    database: "postgres",
    entities: ["src/entidades/*.ts"],
    logging: true,
    synchronize: true,
})
// dev => entities: ["src/entidades/*.ts"]
// prod => entities: ["dist/entidades/*.js"]
/*
export const AppDataSource = new DataSource({
    type: "sqlite",
    database: "./data/angular.db",
    entities: ["src/entidades/*.ts"],
    logging: true,
    synchronize: true,
})
*/
