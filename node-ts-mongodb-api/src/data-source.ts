import { MongoClient, Collection, ObjectId } from 'mongodb';

const mongoUrl = process.env.MONGO_URI || 'mongodb://localhost:27017';
export const dbName = 'cognosdb';
export const client = new MongoClient(mongoUrl);
